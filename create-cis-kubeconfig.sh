#!/usr/bin/env sh

kubectl apply -f https://gitlab.com/gitlab-org/security-products/analyzers/cluster-image-scanning/-/raw/main/gitlab-vulnerability-viewer-service-account.yaml
API_URL=$(kubectl cluster-info | grep -E 'Kubernetes master|Kubernetes control plane' | awk '/http/ {print $NF}')
SECRET=$(kubectl get secret | grep default-token | awk '{print $1}')
CA_CERTIFICATE=$(kubectl get secret "$SECRET" -o jsonpath="{['data']['ca\.crt']}")
VULN_VIEWER_SECRET=$(kubectl -n kube-system get secret | grep gitlab-vulnerability-viewer | awk '{print $1}')
TOKEN=$(kubectl -n kube-system get secret "$VULN_VIEWER_SECRET" -o jsonpath="{.data.token}" | base64 --decode)
echo "
---
apiVersion: v1
kind: Config
clusters:
- name: gitlab-vulnerabilities-viewer
  cluster:
    server: $API_URL
    certificate-authority-data: $CA_CERTIFICATE
contexts:
- name: gitlab-vulnerabilities-viewer
  context:
    cluster: gitlab-vulnerabilities-viewer
    namespace: default
    user: gitlab-vulnerabilities-viewer
current-context: gitlab-vulnerabilities-viewer
users:
- name: gitlab-vulnerabilities-viewer
  user:
    token: $TOKEN
"
